﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Net;
using System.Text.RegularExpressions;
using MySql.Data.MySqlClient;

namespace GetConcept
{
    public class Concept
    {
        readonly List<Language> _allLanguages;

        string _word;
        Language _wordLanguage;

        string _wordWikiText;
        string _languageWikiText;
        string _headerWikiText;

        List<Language> _languages;
        List<Header> _headers;
        List<Meaning> _meanings;
        List<Hyponim> _hyponims;
        List<string> _category;

        string conString = "Database=mediawiki;Data Source=127.0.0.1;User Id=mediawiki_owner;Password=enot;" +
            "default command timeout=0";

        public Concept()
        {
            _languages = new List<Language>();
            _headers = new List<Header>();
            _meanings = new List<Meaning>();
            _hyponims = new List<Hyponim>();
            _category = new List<string>();
        }

        public void FillCategory()
        {
            var fileName = @"C:\Users\Енотик\Desktop\all_russian_nouns.txt";
            _category.Clear();
            List<string> names = new List<string>();
            List<string> allTitles = new List<string>();
            // Переменная для записи результата в файл
            StreamWriter writer = new StreamWriter(fileName, false, Encoding.Default);
            bool cmcont = false;
            string cmcont_num = "";
            do
            {
                var cmwikiText = Parser.GetWikiCategory(cmcont, cmcont_num);

                var cmcontinue = Regex.Matches(cmwikiText, "cmcontinue=\"(page|[0-9]*|[0-9]*)\"");
                if (cmcontinue.Count > 0)
                {
                    cmcont = true;
                    cmcont_num = cmcontinue[0].Groups[1].Value;
                }
                else
                {
                    cmcont = false;
                    cmcont_num = "";
                }

                var all_names = Regex.Matches(cmwikiText, "lh ns=\"[0-9]*\" title=\"([^\"]+)");

                if (all_names.Count == 0)
                    throw new ArgumentException("that category does not contain any pages");

                string str = cmcont ? "+ ..." : "";
                Console.WriteLine($"--found {all_names.Count} pages {str}");

                foreach (Match name in all_names)
                {
                    writer.WriteLine(name.Groups[1].Value);
                    names.Add(name.Groups[1].Value);
                }
            }
            while (cmcont);
            Console.WriteLine("Finished");

            using (MySqlConnection connection = new MySqlConnection(conString))
            {
                connection.Open();
                Console.WriteLine($"Connection is OK!");

                using (MySqlCommand getTitle = new MySqlCommand(@"select page_title from page where page_title " +
                    "rlike '^(А|Б|В|Г|Д|Е|Ж|З|И|Й|К|Л|М|Н|О|П|Р|С|Т|У|Ф|Х|Ц|Ч|Ш|Щ|Ъ|Ы|Ь|Э|Ю|Я|Ё|а|б|в|г|д|е|ж|з|" +
                    "и|й|к|л|м|н|о|п|р|с|т|у|ф|х|ц|ч|ш|щ|ъ|ы|ь|э|ю|я|ё|[0-9])(А|Б|В|Г|Д|Е|Ж|З|И|Й|К|Л|М|Н|О|П|Р|" +
                    "С|Т|У|Ф|Х|Ц|Ч|Ш|Щ|Ъ|Ы|Ь|Э|Ю|Я|Ё|а|б|в|г|д|е|ж|з|и|й|к|л|м|н|о|п|р|с|т|у|ф|х|ц|ч|ш|щ|ъ|ы|ь|э|ю|я|ё|[0-9])*';", connection))
                {
                    MySqlDataReader readTitle = getTitle.ExecuteReader();
                    if (!readTitle.HasRows)
                    {
                        Console.WriteLine($"ERROR Get 0 page_title");
                        readTitle.Close();
                        return;
                    }
                    while (readTitle.Read())
                    {
                        allTitles.Add(readTitle[0].ToString());
                    }
                }
            }
            foreach (var item in names)
            {
                if (allTitles.Contains(item))
                    _category.Add(item);
            }
        }

        //Initializes list of all languages from special file
        private static List<Language> GetAllLanguagesList(string fileName)
        {
            var languages = new List<Language>();
            using (var sr = new StreamReader(fileName))
            {
                while (!sr.EndOfStream)
                {
                    var line = sr.ReadLine();
                    if (line == null) continue;
                    var split = line.Split('\t');
                    languages.Add(new Language(split[1], split[0]));
                }
            }
            return languages;
        }

        private void FillLanguagesForAll()
        {
            _languages.Clear();
            var languagesCodes = Parser.GetLanguageCodes(false, _wordWikiText);
            if (languagesCodes == null)
                throw new ArgumentException();
            foreach (var languageCode in languagesCodes)
            {
                //Search for language with the same code in all languages list and add it to word's languages list
                var language = _allLanguages.Find(lang => lang.Code == languageCode);
                if (language == null)
                    language = new Language(languageCode + " [unknown]", languageCode);
                _languages.Add(language);
            }
            if (!_languages.Exists(lang => lang == _wordLanguage))
                throw new ArgumentException();
        }

        public void FillHeaders(string wikiText)
        {
            _headers.Clear();
            wikiText = wikiText == "" ? _wordWikiText : wikiText;
            int selectedLanguageIndex = _languages.IndexOf(_wordLanguage);
            _languageWikiText = Parser.GetSelectedLanguageWikiText(wikiText, _languages, selectedLanguageIndex);
            _headers = Parser.GetHeaders(_languageWikiText);
        }

        public void SetHyperonimOrHyponim(Relation rel, List<Meaning> temp)
        {
            if (rel == Relation.Hyperonim)
            {
                for (int i = 0; i < temp.Count; i++)
                    _meanings.Add(new Meaning(temp[i].Text, temp[i].Hyperonims));
            }
            else
            {
                for (int i = 0; i < temp.Count; i++)
                    _hyponims.Add(new Hyponim(temp[i].Text, temp[i].Hyperonims));
            }
        }

        public void FillMeanings(int i, Relation rel)
        {
            if (rel == Relation.Hyperonim)
                _meanings.Clear();
            else
                _hyponims.Clear();
            List<Meaning> temp = new List<Meaning>();
            _headerWikiText = Parser.GetSelectedHeaderWikiText(_languageWikiText,
                _headers, i);
            // If there are no headers, use language wikitext
            var wikiText = _headerWikiText.Length == 0 ? _languageWikiText : _headerWikiText;
            var type = Parser.DetectWikiTextType(wikiText);
            switch (type)
            {
                case Parser.WikiTextType.First:
                    temp = Parser.GetMeaningsListT1(rel, wikiText);
                    SetHyperonimOrHyponim(rel, temp);
                    break;
                case Parser.WikiTextType.Second:
                    temp = Parser.GetMeaningsListT2(rel, wikiText);
                    SetHyperonimOrHyponim(rel, temp);
                    break;
                case Parser.WikiTextType.Third:
                    temp = Parser.GetMeaningsListT3(rel, wikiText);
                    SetHyperonimOrHyponim(rel, temp);
                    break;
                case Parser.WikiTextType.Unknown:
                    if (rel == Relation.Hyperonim)
                        _meanings.Clear();
                    else
                        _hyponims.Clear();
                    temp.Clear();
                    throw new ArgumentException(_word + ": unknown wikitext type");
            }
            temp.Clear();
        }

        private void FillWikiTextForAll(string wordWikiText)
        {
            _wordWikiText = wordWikiText;
            var oldWord = _word;
            bool flag = Parser.PageContainsRedirect(ref _word, ref _wordWikiText);
            Parser.RemoveHtmlCode(ref _wordWikiText);
        }

        private string change(string str)
        {
            if (str.Contains('\''))
            {
                string[] arr = str.Split('\'');
                str = "";
                for (int i = 0; i < arr.Length - 1; i++)
                    str = arr[i] + "\\'" + arr[i + 1];
            }
            return str;
        }

        public void GetAllWords()
        {
            try
            {
                FillCategory();
            }
            catch (ArgumentException e)
            {
                Console.WriteLine(e.Message); return;
            }

            List<string> get_all = new List<string>();
            MySqlConnection connection = null;
            _wordLanguage = _allLanguages.Find(lang => lang.Code == "ru");
            StreamWriter writer = new StreamWriter(Properties.Settings.Default.writerFilePath, false, Encoding.Default);

            List<string> result = new List<string>();
            using (connection = new MySqlConnection(conString))
            {
                connection.Open();
                Console.WriteLine($"Connection is OK!");

                foreach (var strTitle in _category)
                {
                    MySqlCommand getRevTextId = new MySqlCommand(@"select rev_text_id from revision left join page on " +
                    " page_latest = rev_id where page_title = '" + change(strTitle) + "'; ", connection);
                    MySqlDataReader readRevTextId = getRevTextId.ExecuteReader();
                    if (!readRevTextId.HasRows)
                    {
                        Console.WriteLine($"ERROR Get 0 rev_text_id");
                        readRevTextId.Close();
                        continue;
                    }
                    string strRevTextId = "(";
                    while (readRevTextId.Read())
                    {
                        strRevTextId += readRevTextId[0] + ", ";
                    }
                    strRevTextId = strRevTextId.Trim();
                    strRevTextId = strRevTextId.Remove(strRevTextId.Length - 1) + ")";
                    readRevTextId.Close();

                    MySqlCommand getPageText = new MySqlCommand(@"select convert(old_text using utf8) from text where " +
                        " old_id in " + strRevTextId + ";", connection);
                    MySqlDataReader readPageText = getPageText.ExecuteReader();
                    if (!readPageText.HasRows)
                    {
                        Console.WriteLine($"ERROR Get 0 old_text");
                        readPageText.Close();
                        continue;
                    }

                    while (readPageText.Read())
                    {
                        _word = strTitle;
                        // WIKITEXT
                        try
                        {
                            FillWikiTextForAll(readPageText[0].ToString());
                        }
                        catch (WebException) // 404
                        {
                            continue;
                        }
                        try
                        {
                            FillLanguagesForAll();
                        }
                        catch (ArgumentException) // no languages
                        {
                            continue;
                        }

                        FillHeaders("");
                        int i = 0;
                        do
                        {
                            try
                            {
                                FillMeanings(i, Relation.Hyperonim);
                                FillMeanings(i, Relation.Hyponim);
                            }
                            catch (ArgumentException)
                            {
                                i++; continue;
                            }

                            for (int j = 0; j < _meanings.Count; j++)
                            {
                                var itemHyperonim = _meanings[j].Hyperonims;
                                // found hyperonim
                                if (itemHyperonim != null)
                                {
                                    var cnt = itemHyperonim.FindAll(x => x != "");
                                    // если в гиперонимах не все - пустые строки
                                    if (cnt.Count != 0)
                                    {
                                        continue;
                                    }
                                }
                                // если гиперонимов нет, то ищем гипонимы
                                var itemHyponim = _hyponims[j].Hyponims;
                                // found hyponim
                                if (itemHyponim != null)
                                {
                                    var cnt = itemHyponim.FindAll(x => x != "");
                                    if (cnt.Count != 0)
                                    {
                                        Console.WriteLine(strTitle);
                                        writer.WriteLine(strTitle);
                                    }
                                }
                            }
                            i++;
                        }
                        while (i < _headers.Count);
                    }
                    readPageText.Close();
                }
            }
            writer.Close();
            Console.WriteLine("Finished");
        }

        public void ClearAll()
        {
            _languages.Clear();
            _headers.Clear();
            _meanings.Clear();
            _hyponims.Clear();
            _category.Clear();
        }

        static void Main(string[] args)
        {
            Concept obj = new Concept();
            obj.GetAllWords();
            obj.ClearAll();
        }
    }
}