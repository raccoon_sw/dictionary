﻿using System.Collections.Generic;

namespace GetConcept
{
    public class Hyponim
    {
        public string Text { get; set; } //name of meaning
        public List<string> Hyponims { get; set; }
        public Hyponim(string text, List<string> hyponims)
        {
            Text = text;
            Hyponims = hyponims;
        }
    }
}
