﻿using Microsoft.Msagl.Drawing;
using System.Collections.Generic;

namespace Dictionary
{
    public class Element
    {
        public int MaxCount { get; set; }
        public List<string> Name { get; set; }
        public Graph Tree { get; set; }
        public Element(int maxCount, List<string> name, Graph tree)
        {
            MaxCount = maxCount;
            Name = name;
            Tree = tree;
        }
    }
}
