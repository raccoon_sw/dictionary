﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using Microsoft.Msagl.Drawing;
using System.Text;
using System.Diagnostics;

namespace Dictionary
{
    class TreeBuilder
    {
        Graph _graph;
        List<Element> _allTrees;
        readonly MainForm _UI;
        readonly PausebleBackgroundWorker _worker;

        public static string _filePathInput;
        string _filePathOutput;
        string _word;
        Language _wordLanguage;

        string _wordWikiText;
        string _languageWikiText;
        string _headerWikiText;

        List<Language> _languages;
        List<Header> _headers;
        List<Meaning> _meanings;
        List<Meaning> _hyperonims;
        public static string _nouns;
        private string _nounsCopy;
        List<string> _maxNouns;

        Node _lastSelectedNode;
        bool _firstWord;
        int k;
        int maxAll;

        public static string Nouns
        {
            get { return _nouns; }
        }

        public TreeBuilder(MainForm UI)
        {
            _UI = UI;
            _worker = new PausebleBackgroundWorker();
            _worker.DoWork += DoWork;
            _worker.RunWorkerCompleted += RunWorkerCompleted;
            _worker.WorkerSupportsCancellation = true;
            _UI.SetGraphMouseClickEvent(Code.ForTree, GraphMouseClick);
            _wordLanguage = GraphBuilder.AllLanguages.Find(x => x.Code == "ru");
            _languages = new List<Language>();
            _headers = new List<Header>();
            _meanings = new List<Meaning>();
            _hyperonims = new List<Meaning>();
            _nouns = "";
            _allTrees = new List<Element>();
            _maxNouns = new List<string>();
            maxAll = 0;
        }

        private string ReadString(string filePath)
        {
            var result = "";
            using (StreamReader reader = new StreamReader(filePath, Encoding.Default))
            {
                var text = "";
                while ((text = reader.ReadLine()) != null)
                    result += text;
            }
            return result;
        }

        private void TextToUTF()
        {
            var result = ReadString(_filePathInput);
            using (StreamWriter writer = new StreamWriter(_filePathInput, false, Encoding.UTF8))
            {
                writer.WriteLine(result);
            }
        }

        private void DoWordsNormalize()
        {
            TextToUTF();
            var idx = _filePathInput.LastIndexOf(@".");
            var beginStr = _filePathInput.Remove(idx, _filePathInput.Length - idx);
            _filePathOutput = beginStr + "_output.txt";
            StreamWriter writer = new StreamWriter(_filePathOutput, false, Encoding.Default);
            writer.Flush();
            writer.Close();
            // создаем процесс cmd.exe с параметром "/c",
            // где Properties.Settings.Default.PathMyStem, _filePathInput, _filePathOutput
            // пути до MyStem, выбранного файла, результирующего файла соответсвенно
            ProcessStartInfo psiOpt = new ProcessStartInfo(@"cmd.exe",
                @"/c " + Properties.Settings.Default.PathMyStem + " " + _filePathInput + " " + _filePathOutput + " -cli");
            // скрываем окно запущенного процесса C:\mystem\mystem.exe
            psiOpt.CreateNoWindow = false;
            psiOpt.WindowStyle = ProcessWindowStyle.Hidden;
            psiOpt.RedirectStandardOutput = true;
            psiOpt.UseShellExecute = false;
            // запускаем процесс
            Process procCommand = Process.Start(psiOpt);
            // получаем ответ запущенного процесса
            StreamReader srIncoming = procCommand.StandardOutput;
            // закрываем процесс
            procCommand.WaitForExit();
        }

        private void DeleteFile()
        {
            // создаем процесс cmd.exe
            ProcessStartInfo psiOpt = new ProcessStartInfo(@"cmd.exe",
                @"/c DEL /F /S /Q /A " + "\"" + _filePathOutput + "\"");
            // скрываем окно запущенного процесса
            psiOpt.CreateNoWindow = false;
            psiOpt.WindowStyle = ProcessWindowStyle.Hidden;
            psiOpt.RedirectStandardOutput = true;
            psiOpt.UseShellExecute = false;
            // запускаем процесс
            Process procCommand = Process.Start(psiOpt);
            // получаем ответ запущенного процесса
            StreamReader srIncoming = procCommand.StandardOutput;
            // закрываем процесс
            procCommand.WaitForExit();
            _filePathOutput = "";
        }

        public bool ParseText()
        {
            _UI.AddToLog(Code.ForTree, "adding nouns from text");
            DoWordsNormalize();
            var wordsText = ReadString(_filePathOutput);
            var allWords = Regex.Matches(wordsText, "{([А-Яа-яЁё]+)=S,");

            if (allWords.Count == 0)
            {
                _UI.AddToLog(Code.ForTree, "there is no nouns");
                return false;
            }
            // check each word in list
            foreach (Match word in allWords)
            {
                var noun = word.Groups[1].Value.Trim();
                // check exists nouns in the wiki
                if (!GraphBuilder.AllNouns.Contains(noun))
                {
                    _UI.AddToLog(Code.ForTree, $"in all_nouns doesn't exist word: {noun}");
                }
                else if (!_nouns.Contains(noun + ";"))
                {
                    _nouns += noun + ";";
                    _UI.AddToLog(Code.ForTree, $"-- {noun}");
                }
            }
            _UI.AddToLog(Code.ForTree, "added all nouns from text");
            _nounsCopy = _nouns;
            DeleteFile();
            return true;
        }

        public void RunWorker(string word, bool updatingMeaning)
        {
            // Abort current run first
            if (_worker.IsBusy)
            {
                var dialogResult = MessageBox.Show(@"Abort building and start a new tree?",
                    @"Warning", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                if (dialogResult == DialogResult.No)
                {
                    _UI.RedrawGraph(Code.ForTree, _graph);
                    return;
                }
                _worker.CancelAsync();
                _worker.Resume();
            }
            _firstWord = !updatingMeaning;
            _UI.AddToLog(Code.ForTree, "\n");
            _UI.AddToLog(Code.ForTree, "Create new tree:");
            if (!_worker.IsBusy)
            {
                _worker.RunWorkerAsync(word);
            }
        }

        public void ResumeWorker() => _worker.Resume();

        private void DoWork(object sender, DoWorkEventArgs e)
        {
            AddWord(e.Argument);
            e.Cancel = _worker.CancellationPending;
        }

        private void RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (e.Cancelled)
            {
                RunWorker(_word, false);
            }
            else
            {
                _allTrees.Add(new Element(_maxNouns.Count, _maxNouns, _graph));
                maxAll = _maxNouns.Count > maxAll ? _maxNouns.Count : maxAll;
                if (_nouns == "")
                {
                    Element res = null;
                    int maxCNode = 10000000;
                    foreach (var item in _allTrees)
                    {
                        if (item.MaxCount == maxAll && item.Tree.NodeCount < maxCNode)
                        {
                            res = item;
                            maxCNode = item.Tree.NodeCount;
                        }
                    }
                    _UI.RedrawGraph(Code.ForTree, res.Tree);
                    _allTrees.Clear();
                    _nounsCopy = "";
                    _filePathInput = "";
                    _filePathOutput = "";
                    MessageBox.Show(@"Finished building");
                }
            }
            _word = "";
            _wordWikiText = "";
            _languageWikiText = "";
            _headerWikiText = "";
            _languages.Clear();
            _headers.Clear();
            _meanings.Clear();
            _hyperonims.Clear();
            _maxNouns.Clear();
            if (_nouns != "")
            {
                RunWorker(_nouns.Split(';')[0], false);
                ResumeWorker();
            }
        }

        private void GraphMouseClick(object sender, MouseEventArgs args)
        {
            if (_UI.GetGraphSelectedObject(Code.ForTree) is Node)
                _lastSelectedNode = (Node)_UI.GetGraphSelectedObject(Code.ForTree);
            else
            {
                _lastSelectedNode = null;
                return;
            }
        }
        
        private void DeleteSubstring(string pattern)
        {
            var idx = _nouns.IndexOf(pattern);
            if (idx == -1)
                return;
            _maxNouns.Add(pattern);
            _nouns = _nouns.Remove(idx, pattern.Length + 1);
        }

        // берет от слова его гиперонимы и удаляет из всех существительных слова, которые есть в гиперонимах
        private void GetHiperonimFromWord()
        {
            string temp = _word;
            _hyperonims.Clear();
            DeleteSubstring(temp);
            string wikiText;
            wikiText = Parser.GetWikiText(temp);
            bool smt = Parser.PageContainsRedirect(ref temp, ref wikiText);

            // проверим есть ли такая статья в категории Русские существительные
            if (!GraphBuilder.AllNouns.Contains(temp))
            {
                throw new ArgumentException($"in all_nouns doesn't exist word: {temp}");
            }
            FillLanguages(wikiText);
            FillHeaders(wikiText);

            int i = 0;
            List<string> forHyper = new List<string>();
            do
            {
                try
                {
                    FillMeanings(i);
                }
                catch (ArgumentException)
                {
                    i++; continue;
                }

                if (_meanings == null)
                {
                    i++; continue;
                }
                for (int j = 0; j < _meanings.Count; j++)
                {
                    var item = _meanings[j].Hyperonims;
                    if (item == null) //если не нашли
                    {
                        continue;
                    }
                    foreach (var hyper in item)
                    {
                        if (hyper == "")
                            continue;
                        // проверим есть ли такая статья в категории Русские существительные
                        if (!GraphBuilder.AllNouns.Contains(hyper))
                            continue;
                        if (!forHyper.Contains(hyper))
                        {
                            forHyper.Add(hyper);
                            _UI.AddToLog(Code.ForTree, $"-- {hyper}");
                        }
                    }
                }
                // сначала проверим есть ли там вообще нужные существительные
                foreach (var word in _nounsCopy.Split(';'))
                {
                    if (word == "")
                        continue;
                    var tmp = word;
                    if (forHyper.Contains(tmp))
                    {
                        if (_nouns.Contains((tmp + ";")))
                        {
                            DeleteSubstring(tmp);
                        }
                        if (!_maxNouns.Contains(tmp))
                            _maxNouns.Add(tmp);
                    }
                }
                i++;
            }
            while (i < _headers.Count);
            _hyperonims.Add(new Meaning(temp, forHyper));
        }

        private void AddWord(object objectWord)
        {
            _word = (string)objectWord;
            // WIKITEXT
            try
            {
                FillWikiText();
            }
            catch (WebException e) // 404
            {
                _UI.AddToLog(Code.ForTree, $"{_word}: {e.Message}");
                if (_firstWord)
                    return;
                _graph.FindNode(_word).Attr.FillColor = Color.Gray;
                _UI.RedrawGraph(Code.ForTree, _graph);
                return;
            }
            try
            {
                if (_firstWord)
                {
                    // LANGUAGES
                    FillLanguages("");
                    _UI.RedrawGraph(Code.ForTree, null);
                    if (_allTrees.Count == 0)
                        _worker.Wait(); // Приостановить фоновый поток
                    FillHeaders("");
                }
            }
            catch (ArgumentException e) // no languages
            {
                _UI.AddToLog(Code.ForTree, $"{_word}: {e.Message}");
                if (_firstWord)
                    return;
                _graph.FindNode(_word).Attr.FillColor = Color.Gray;
                _UI.RedrawGraph(Code.ForTree, _graph);
                return;
            }
            // MEANINGS
            try
            {
                if (_firstWord)
                {
                    FillMeanings(0);
                    _UI.AddToLog(Code.ForTree, $": {_meanings[0].Text}");
                }
                GetHiperonimFromWord();
            }
            catch (WebException e) // 404
            {
                _UI.AddToLog(Code.ForTree, $"{_word}: {e.Message}");
                if (_firstWord)
                    return;
                _graph.FindNode(_word).Attr.FillColor = Color.Gray;
                _UI.RedrawGraph(Code.ForTree, _graph);
                return;
            }
            catch (ArgumentException e) // unknown wikitext type or can't parse known ones
            {
                _UI.AddToLog(Code.ForTree, $"{_word}: {e.Message}");
                _UI.LanguagesEnabled(false);
                if (!_firstWord)
                {
                    _graph.FindNode(_word).Attr.FillColor = Color.Gray;
                    _UI.RedrawGraph(Code.ForTree, _graph);
                    return;
                }
            }

            // Add start word
            if (_firstWord)
            {
                k = 0;
                _graph = new Graph();
                _graph.AddNode(_word);
                _UI.RedrawGraph(Code.ForTree, _graph);
                _UI.LanguagesEnabled(false);
                _firstWord = false;
                k = 1;
            }
            var node = _graph.FindNode(_word);
            // Stop current building if new graph building started
            if (_worker.CancellationPending)
                return;

            // Add UserData
            if (k == 1 && _meanings.Count != 0)
            {
                node.UserData = _meanings[0].Text;
                k = 0;
            }
            if (_hyperonims.Count != 0)
            {
                // Clear node color
                node.Attr.FillColor = Color.White;
                _UI.RedrawGraph(Code.ForTree, _graph);
                AddAllHyperonims();
            }
            else
            {
                node.Attr.FillColor = Color.Gray;
                _UI.RedrawGraph(Code.ForTree, _graph);
            }
        }

        private void AddAllHyperonims()
        {
            var parentWord = _word;
            int i = 0;
            foreach (var hyperonim in _hyperonims[0].Hyperonims)
            {
                if (hyperonim.Length == 0) continue;
                if (_graph.FindNode(hyperonim) != null)
                {
                    if (!EdgeExists(_graph, parentWord, hyperonim))
                    {
                        _graph.AddEdge(parentWord, hyperonim);
                        _UI.RedrawGraph(Code.ForTree, _graph);
                    }
                    continue;
                }
                _graph.AddNode(hyperonim);                
                _graph.AddEdge(parentWord, hyperonim);
                _UI.RedrawGraph(Code.ForTree, _graph);
                AddWord(hyperonim);
                i++;
                if (_worker.CancellationPending) return;
            }
        }
        
        private void FillWikiText()
        {
            _wordWikiText = Parser.GetWikiText(_word);
            var oldWord = _word;
            if (Parser.PageContainsRedirect(ref _word, ref _wordWikiText))
            {
                _UI.AddToLog(Code.ForTree, $"{oldWord}: redirect -> {_word}");
                // Change early added node
                if (!_firstWord)
                {
                    var node = _graph.FindNode(oldWord);
                    var sources = new List<string>();
                    foreach (var inEdge in node.InEdges.ToList())
                    {
                        sources.Add(inEdge.Source);
                        _graph.RemoveEdge(inEdge);
                    }
                    _graph.NodeMap.Remove(node.Id);
                    _graph.GeometryGraph.Nodes.Remove(node.GeometryObject as Microsoft.Msagl.Core.Layout.Node);
                    _graph.AddNode(_word);
                    foreach (var source in sources)
                    {
                        _graph.AddEdge(source, _word);
                    }
                    _UI.RedrawGraph(Code.ForTree, _graph);
                }
            }
            Parser.RemoveHtmlCode(ref _wordWikiText);
            _UI.AddToLog(Code.ForTree, $"{_word}: choose meaning");
        }

        private void FillLanguages(string wikiText)
        {
            _languages.Clear();
            bool flag = wikiText == "" ? true : false;
            wikiText = flag ? _wordWikiText : wikiText;
            var languagesCodes = Parser.GetLanguageCodes(false, wikiText);
            if (!flag && languagesCodes == null)
                throw new ArgumentException();
            foreach (var languageCode in languagesCodes)
            {
                //Search for language with the same code in all languages list and add it to word's languages list
                var language = GraphBuilder.AllLanguages.Find(lang => lang.Code == languageCode);
                if (language == null)
                    language = new Language(languageCode + " [unknown]", languageCode);
                _languages.Add(language);
            }
            if (!flag && !_languages.Exists(lang => lang == _wordLanguage))
                throw new ArgumentException();
        }

        private void FillHeaders(string wikiText)
        {
            _headers.Clear();
            wikiText = wikiText == "" ? _wordWikiText : wikiText;
            int selectedLanguageIndex = _languages.IndexOf(_wordLanguage);
            _languageWikiText = Parser.GetSelectedLanguageWikiText(wikiText, _languages, selectedLanguageIndex);
            _headers = Parser.GetHeaders(_languageWikiText);
        }

        private void Show(int count, MatchCollection links)
        {
            if (count % 50 == 0 || count == links.Count)
                _UI.AddToLog(Code.ForTree, $"--viewed {count} links");
        }
        
        public void FillMeanings(int i)
        {
            _meanings.Clear();
            _headerWikiText = Parser.GetSelectedHeaderWikiText(_languageWikiText,
                _headers, i);
            // If there are no headers, use language wikitext
            var wikiText = _headerWikiText.Length == 0 ? _languageWikiText : _headerWikiText;
            var type = Parser.DetectWikiTextType(wikiText);
            switch (type)
            {
                case Parser.WikiTextType.First:
                    _meanings = Parser.GetMeaningsListT1(Relation.Hyperonim, wikiText);
                    break;
                case Parser.WikiTextType.Second:
                    _meanings = Parser.GetMeaningsListT2(Relation.Hyperonim, wikiText);
                    break;
                case Parser.WikiTextType.Third:
                    _meanings = Parser.GetMeaningsListT3(Relation.Hyperonim, wikiText);
                    break;
                case Parser.WikiTextType.Unknown:
                    _meanings.Clear();
                    throw new ArgumentException(_word + ": unknown wikitext type");
            }
        }        
        
        /// <summary>
        /// Checks if edge (both directions) exists in graph
        /// </summary>
        /// <param name="graph">Graph reference</param>
        /// <param name="first">First node id</param>
        /// <param name="second">Second node id</param>
        /// <returns></returns>
        private bool EdgeExists(Graph graph, string first, string second)
        {
            return graph.Edges.Any(edge => edge.Source == first && edge.Target == second ||
            edge.Source == second && edge.Target == first);
        }
        
    }
}
