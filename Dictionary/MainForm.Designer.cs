﻿namespace Dictionary
{
    partial class MainForm
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.gbWord = new System.Windows.Forms.GroupBox();
            this.lblConcept = new System.Windows.Forms.Label();
            this.cbConcept = new System.Windows.Forms.ComboBox();
            this.btnSelect = new System.Windows.Forms.Button();
            this.btnGO = new System.Windows.Forms.Button();
            this.tbWord = new System.Windows.Forms.TextBox();
            this.lblLang = new System.Windows.Forms.Label();
            this.cbLang = new System.Windows.Forms.ComboBox();
            this.gbGraph = new System.Windows.Forms.GroupBox();
            this.gbLog = new System.Windows.Forms.GroupBox();
            this.tbLog = new System.Windows.Forms.TextBox();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.gbText = new System.Windows.Forms.GroupBox();
            this.btnGetPath = new System.Windows.Forms.Button();
            this.lblPath = new System.Windows.Forms.Label();
            this.btnSelectText = new System.Windows.Forms.Button();
            this.tbPath = new System.Windows.Forms.TextBox();
            this.gbTree = new System.Windows.Forms.GroupBox();
            this.gbLogText = new System.Windows.Forms.GroupBox();
            this.tbLogText = new System.Windows.Forms.TextBox();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.pausebleBackgroundWorker1 = new Dictionary.PausebleBackgroundWorker();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.gbWord.SuspendLayout();
            this.gbLog.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.gbText.SuspendLayout();
            this.gbLogText.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Location = new System.Drawing.Point(2, 1);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(880, 551);
            this.tabControl1.TabIndex = 1;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.gbWord);
            this.tabPage1.Controls.Add(this.gbGraph);
            this.tabPage1.Controls.Add(this.gbLog);
            this.tabPage1.Location = new System.Drawing.Point(4, 25);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(872, 522);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "From word";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // gbWord
            // 
            this.gbWord.Controls.Add(this.lblConcept);
            this.gbWord.Controls.Add(this.cbConcept);
            this.gbWord.Controls.Add(this.btnSelect);
            this.gbWord.Controls.Add(this.btnGO);
            this.gbWord.Controls.Add(this.tbWord);
            this.gbWord.Controls.Add(this.lblLang);
            this.gbWord.Controls.Add(this.cbLang);
            this.gbWord.Location = new System.Drawing.Point(4, 4);
            this.gbWord.Margin = new System.Windows.Forms.Padding(4);
            this.gbWord.Name = "gbWord";
            this.gbWord.Padding = new System.Windows.Forms.Padding(4);
            this.gbWord.Size = new System.Drawing.Size(322, 177);
            this.gbWord.TabIndex = 10;
            this.gbWord.TabStop = false;
            this.gbWord.Text = "word";
            // 
            // lblConcept
            // 
            this.lblConcept.AutoSize = true;
            this.lblConcept.Location = new System.Drawing.Point(5, 94);
            this.lblConcept.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblConcept.Name = "lblConcept";
            this.lblConcept.Size = new System.Drawing.Size(69, 17);
            this.lblConcept.TabIndex = 9;
            this.lblConcept.Text = "concepts:";
            // 
            // cbConcept
            // 
            this.cbConcept.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbConcept.FormattingEnabled = true;
            this.cbConcept.Location = new System.Drawing.Point(85, 91);
            this.cbConcept.Margin = new System.Windows.Forms.Padding(4);
            this.cbConcept.Name = "cbConcept";
            this.cbConcept.Size = new System.Drawing.Size(229, 24);
            this.cbConcept.TabIndex = 3;
            this.cbConcept.SelectedIndexChanged += new System.EventHandler(this.cbConcept_SelectedIndexChanged);
            // 
            // btnSelect
            // 
            this.btnSelect.Location = new System.Drawing.Point(6, 123);
            this.btnSelect.Margin = new System.Windows.Forms.Padding(4);
            this.btnSelect.Name = "btnSelect";
            this.btnSelect.Size = new System.Drawing.Size(308, 43);
            this.btnSelect.TabIndex = 4;
            this.btnSelect.Text = "SELECT";
            this.btnSelect.UseVisualStyleBackColor = true;
            this.btnSelect.Click += new System.EventHandler(this.btnSelect_Click);
            // 
            // btnGO
            // 
            this.btnGO.Location = new System.Drawing.Point(224, 22);
            this.btnGO.Margin = new System.Windows.Forms.Padding(4);
            this.btnGO.Name = "btnGO";
            this.btnGO.Size = new System.Drawing.Size(91, 28);
            this.btnGO.TabIndex = 1;
            this.btnGO.Text = "new tree";
            this.btnGO.UseVisualStyleBackColor = true;
            this.btnGO.Click += new System.EventHandler(this.btnGO_Click);
            // 
            // tbWord
            // 
            this.tbWord.Location = new System.Drawing.Point(8, 25);
            this.tbWord.Margin = new System.Windows.Forms.Padding(4);
            this.tbWord.Name = "tbWord";
            this.tbWord.Size = new System.Drawing.Size(208, 22);
            this.tbWord.TabIndex = 0;
            this.tbWord.Enter += new System.EventHandler(this.tbWord_Enter);
            // 
            // lblLang
            // 
            this.lblLang.AutoSize = true;
            this.lblLang.Location = new System.Drawing.Point(5, 63);
            this.lblLang.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblLang.Name = "lblLang";
            this.lblLang.Size = new System.Drawing.Size(71, 17);
            this.lblLang.TabIndex = 1;
            this.lblLang.Text = "language:";
            // 
            // cbLang
            // 
            this.cbLang.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbLang.FormattingEnabled = true;
            this.cbLang.Location = new System.Drawing.Point(85, 60);
            this.cbLang.Margin = new System.Windows.Forms.Padding(4);
            this.cbLang.Name = "cbLang";
            this.cbLang.Size = new System.Drawing.Size(229, 24);
            this.cbLang.TabIndex = 2;
            this.cbLang.SelectionChangeCommitted += new System.EventHandler(this.cbLang_SelectionChangeCommitted);
            // 
            // gbGraph
            // 
            this.gbGraph.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gbGraph.Location = new System.Drawing.Point(334, 4);
            this.gbGraph.Margin = new System.Windows.Forms.Padding(4);
            this.gbGraph.Name = "gbGraph";
            this.gbGraph.Padding = new System.Windows.Forms.Padding(4);
            this.gbGraph.Size = new System.Drawing.Size(529, 514);
            this.gbGraph.TabIndex = 12;
            this.gbGraph.TabStop = false;
            this.gbGraph.Text = "tree";
            // 
            // gbLog
            // 
            this.gbLog.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.gbLog.Controls.Add(this.tbLog);
            this.gbLog.Location = new System.Drawing.Point(4, 189);
            this.gbLog.Margin = new System.Windows.Forms.Padding(4);
            this.gbLog.Name = "gbLog";
            this.gbLog.Padding = new System.Windows.Forms.Padding(4);
            this.gbLog.Size = new System.Drawing.Size(322, 329);
            this.gbLog.TabIndex = 11;
            this.gbLog.TabStop = false;
            this.gbLog.Text = "log";
            // 
            // tbLog
            // 
            this.tbLog.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbLog.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.tbLog.Location = new System.Drawing.Point(12, 23);
            this.tbLog.Margin = new System.Windows.Forms.Padding(4);
            this.tbLog.Multiline = true;
            this.tbLog.Name = "tbLog";
            this.tbLog.ReadOnly = true;
            this.tbLog.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.tbLog.Size = new System.Drawing.Size(302, 298);
            this.tbLog.TabIndex = 8;
            this.tbLog.TabStop = false;
            this.tbLog.TextChanged += new System.EventHandler(this.tbLog_TextChanged);
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.gbText);
            this.tabPage2.Controls.Add(this.gbTree);
            this.tabPage2.Controls.Add(this.gbLogText);
            this.tabPage2.Location = new System.Drawing.Point(4, 25);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(872, 522);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "From text";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // gbText
            // 
            this.gbText.Controls.Add(this.btnGetPath);
            this.gbText.Controls.Add(this.lblPath);
            this.gbText.Controls.Add(this.btnSelectText);
            this.gbText.Controls.Add(this.tbPath);
            this.gbText.Location = new System.Drawing.Point(6, 4);
            this.gbText.Margin = new System.Windows.Forms.Padding(4);
            this.gbText.Name = "gbText";
            this.gbText.Padding = new System.Windows.Forms.Padding(4);
            this.gbText.Size = new System.Drawing.Size(322, 144);
            this.gbText.TabIndex = 13;
            this.gbText.TabStop = false;
            this.gbText.Text = "text";
            // 
            // btnGetPath
            // 
            this.btnGetPath.Location = new System.Drawing.Point(6, 23);
            this.btnGetPath.Margin = new System.Windows.Forms.Padding(4);
            this.btnGetPath.Name = "btnGetPath";
            this.btnGetPath.Size = new System.Drawing.Size(310, 29);
            this.btnGetPath.TabIndex = 0;
            this.btnGetPath.Text = "select file path";
            this.btnGetPath.UseVisualStyleBackColor = true;
            this.btnGetPath.Click += new System.EventHandler(this.btnGetPath_Click);
            // 
            // lblPath
            // 
            this.lblPath.AutoSize = true;
            this.lblPath.Location = new System.Drawing.Point(3, 63);
            this.lblPath.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblPath.Name = "lblPath";
            this.lblPath.Size = new System.Drawing.Size(40, 17);
            this.lblPath.TabIndex = 8;
            this.lblPath.Text = "path:";
            // 
            // btnSelectText
            // 
            this.btnSelectText.Location = new System.Drawing.Point(6, 91);
            this.btnSelectText.Margin = new System.Windows.Forms.Padding(4);
            this.btnSelectText.Name = "btnSelectText";
            this.btnSelectText.Size = new System.Drawing.Size(310, 43);
            this.btnSelectText.TabIndex = 1;
            this.btnSelectText.Text = "SELECT";
            this.btnSelectText.UseVisualStyleBackColor = true;
            this.btnSelectText.Click += new System.EventHandler(this.btnSelectText_Click);
            // 
            // tbPath
            // 
            this.tbPath.Location = new System.Drawing.Point(51, 60);
            this.tbPath.Margin = new System.Windows.Forms.Padding(4);
            this.tbPath.Name = "tbPath";
            this.tbPath.ReadOnly = true;
            this.tbPath.Size = new System.Drawing.Size(265, 22);
            this.tbPath.TabIndex = 3;
            // 
            // gbTree
            // 
            this.gbTree.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gbTree.Location = new System.Drawing.Point(336, 4);
            this.gbTree.Margin = new System.Windows.Forms.Padding(4);
            this.gbTree.Name = "gbTree";
            this.gbTree.Padding = new System.Windows.Forms.Padding(4);
            this.gbTree.Size = new System.Drawing.Size(527, 512);
            this.gbTree.TabIndex = 15;
            this.gbTree.TabStop = false;
            this.gbTree.Text = "tree";
            // 
            // gbLogText
            // 
            this.gbLogText.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.gbLogText.Controls.Add(this.tbLogText);
            this.gbLogText.Location = new System.Drawing.Point(6, 156);
            this.gbLogText.Margin = new System.Windows.Forms.Padding(4);
            this.gbLogText.Name = "gbLogText";
            this.gbLogText.Padding = new System.Windows.Forms.Padding(4);
            this.gbLogText.Size = new System.Drawing.Size(322, 360);
            this.gbLogText.TabIndex = 14;
            this.gbLogText.TabStop = false;
            this.gbLogText.Text = "log";
            // 
            // tbLogText
            // 
            this.tbLogText.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbLogText.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.tbLogText.Location = new System.Drawing.Point(12, 23);
            this.tbLogText.Margin = new System.Windows.Forms.Padding(4);
            this.tbLogText.Multiline = true;
            this.tbLogText.Name = "tbLogText";
            this.tbLogText.ReadOnly = true;
            this.tbLogText.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.tbLogText.Size = new System.Drawing.Size(302, 329);
            this.tbLogText.TabIndex = 8;
            this.tbLogText.TabStop = false;
            this.tbLogText.TextChanged += new System.EventHandler(this.tbLogText_TextChanged);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "yourFilePath";
            this.openFileDialog1.RestoreDirectory = true;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(882, 553);
            this.Controls.Add(this.tabControl1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimumSize = new System.Drawing.Size(900, 600);
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Ideographic dictionary";
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.gbWord.ResumeLayout(false);
            this.gbWord.PerformLayout();
            this.gbLog.ResumeLayout(false);
            this.gbLog.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.gbText.ResumeLayout(false);
            this.gbText.PerformLayout();
            this.gbLogText.ResumeLayout(false);
            this.gbLogText.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.GroupBox gbWord;
        private System.Windows.Forms.Label lblConcept;
        private System.Windows.Forms.ComboBox cbConcept;
        public System.Windows.Forms.Button btnSelect;
        private System.Windows.Forms.Button btnGO;
        private System.Windows.Forms.TextBox tbWord;
        private System.Windows.Forms.Label lblLang;
        private System.Windows.Forms.ComboBox cbLang;
        private System.Windows.Forms.GroupBox gbGraph;
        private System.Windows.Forms.GroupBox gbLog;
        private System.Windows.Forms.TextBox tbLog;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.GroupBox gbText;
        public System.Windows.Forms.Button btnSelectText;
        private System.Windows.Forms.GroupBox gbTree;
        private System.Windows.Forms.GroupBox gbLogText;
        private System.Windows.Forms.TextBox tbLogText;
        private PausebleBackgroundWorker pausebleBackgroundWorker1;
        private System.Windows.Forms.TextBox tbPath;
        private System.Windows.Forms.Label lblPath;
        public System.Windows.Forms.Button btnGetPath;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
    }
}

