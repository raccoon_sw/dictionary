﻿using System.ComponentModel;
using System.Threading;

namespace Dictionary
{
    public class PausebleBackgroundWorker : BackgroundWorker
    {
        private readonly AutoResetEvent _locker = new AutoResetEvent(false);
        private bool _waiting;

        public void Wait()
        {
            _waiting = true;
            _locker.WaitOne();
        }

        public void Resume()
        {
            if (!_waiting) return;
            _waiting = false;
            _locker.Set();
        }
    }
}
