﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using Microsoft.Msagl.Drawing;
using Microsoft.Msagl.GraphViewerGdi;
using System.Text.RegularExpressions;

namespace Dictionary
{
    public partial class MainForm : Form
    {
        private readonly GViewer _gViewer1, _gViewer2;
        private readonly GraphBuilder _graphBuilder;
        private readonly TreeBuilder _treeBuilder;
        public static bool getWordOrConcept;

        public string Word
        {
            get { return (string)tbWord.Invoke(new Func<string>(() => tbWord.Text)); }
            set { tbWord.Invoke(new Action(() => tbWord.Text = value)); }
        }
        
        public string Concept
        {
            get { return (string)cbConcept.Invoke(new Func<string>(() => cbConcept.Text)); }
        }

        public void SetLanguages(List<Language> languages)
        {
            cbLang.Invoke(new Action(() =>
            {
                cbLang.DataSource = null;
                cbLang.DataSource = languages;
                if (cbLang.Items.Count != 0)
                    cbLang.SelectedIndex = 0;
                cbLang.ValueMember = "Code";
                cbLang.DisplayMember = "Name";
                cbLang.Enabled = cbLang.Items.Count != 0;
            }));
        }

        public void SetConcepts(List<string> concepts)
        {
            if (cbConcept.Items.Count != 0)
                cbConcept.SelectedIndex = 0;
            cbConcept.DataSource = null;
            cbConcept.DataSource = concepts;
                cbConcept.Enabled = cbConcept.Items.Count != 0;
        }

        public int SelectedLanguageIndex => (int)cbLang.Invoke(new Func<int>(() => cbLang.SelectedIndex));

        public void LanguagesEnabled(bool enabled) => cbLang.Invoke(new Action(() => cbLang.Enabled = enabled));

        public void SelectButtonEnabled(bool enabled) => btnSelect.Invoke(new Action(() => btnSelect.Enabled = enabled));

        // code == ForGraph => AddToLog, code == ForTree => AddToLogText
        public void AddToLog(Code code, string text) =>
            (code == Code.ForGraph ? tbLog : tbLogText).Invoke(text.Length == 0
            ? new Action(() => (code == Code.ForGraph ? tbLog : tbLogText).Clear())
            : new Action(() => (code == Code.ForGraph ? tbLog : tbLogText).Text += text + Environment.NewLine));
        
        // code == ForGraph => _gViewer1, code == ForTree => _gViewer2
        public void RedrawGraph(Code code, Graph graph) =>
            (code == Code.ForGraph ? _gViewer1 : _gViewer2).Invoke(new Action(() => (code == Code.ForGraph ? _gViewer1 : _gViewer2).Graph = graph));

        public delegate void GraphMouseClick(object sender, MouseEventArgs args);
        
        public void SetGraphMouseClickEvent(Code code, GraphMouseClick graphMouseClick) =>
            (code == Code.ForGraph ? _gViewer1 : _gViewer2).MouseClick += graphMouseClick.Invoke;
        
        public object GetGraphSelectedObject(Code code) => (code == Code.ForGraph ? _gViewer1 : _gViewer2).SelectedObject;
        
        public MainForm()
        {
            InitializeComponent();
            openFileDialog1.Filter = "Text (*.txt)|*.txt";
            getWordOrConcept = false; // select Word
            // page 1
            _gViewer1 = new GViewer
            {
                Dock = DockStyle.Fill,
                TabStop = false,
                OutsideAreaBrush = Brushes.White,
                EdgeInsertButtonVisible = false,
                LayoutAlgorithmSettingsButtonVisible = false,
                NavigationVisible = false,
                PanButtonPressed = true
            };
            gbGraph.SuspendLayout();
            gbGraph.Controls.Add(_gViewer1);
            gbGraph.ResumeLayout();
            _graphBuilder = new GraphBuilder(this, @"all_langs.txt", @"all_nouns.txt", @"selected.txt");
            cbLang.Enabled = false;
            btnSelect.Enabled = false;

            //page 2
            _gViewer2 = new GViewer
            {
                Dock = DockStyle.Fill,
                TabStop = false,
                OutsideAreaBrush = Brushes.White,
                EdgeInsertButtonVisible = false,
                LayoutAlgorithmSettingsButtonVisible = false,
                NavigationVisible = false,
                PanButtonPressed = true
            };
            gbTree.SuspendLayout();
            gbTree.Controls.Add(_gViewer2);
            gbTree.ResumeLayout();
            _treeBuilder = new TreeBuilder(this);
            btnSelectText.Enabled = false;
        }
        
        private void cbLang_SelectionChangeCommitted(object sender, EventArgs e)
        {
            if (_gViewer1.Graph == null)
                return;
        }

        private void tbWord_Enter(object sender, EventArgs e)
        {
            tbWord.SelectionStart = 0;
            tbWord.SelectionLength = tbWord.TextLength;
        }

        private void tbLog_TextChanged(object sender, EventArgs e)
        {
            tbLog.SelectionStart = tbLog.Text.Length;
            tbLog.ScrollToCaret();
        }
                
        private void btnGO_Click(object sender, EventArgs e)
        {
            if (cbConcept.SelectedIndex != 0)
                _graphBuilder.RunWorker(cbConcept.Text, false);
            else
            {
                if (tbWord.Text.Length == 0)
                    tbWord.Focus();
                else
                    _graphBuilder.RunWorker(tbWord.Text, false);
            }
            btnSelect.Enabled = true;
        }

        private void btnSelect_Click(object sender, EventArgs e)
        {
            btnSelect.Enabled = false;
            _graphBuilder.ResumeWorker();
        }

        private void btnSelectText_Click(object sender, EventArgs e)
        {
            btnSelectText.Enabled = false;
            _treeBuilder.ResumeWorker();
        }

        private void tbLogText_TextChanged(object sender, EventArgs e)
        {
            tbLogText.SelectionStart = tbLogText.Text.Length;
            tbLogText.ScrollToCaret();
        }

        private void cbConcept_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cbConcept.SelectedIndex == 0)
                getWordOrConcept = false;
            getWordOrConcept = true;
        }

        private void btnGetPath_Click(object sender, EventArgs e)
        {
            try
            {
                if (openFileDialog1.ShowDialog() == DialogResult.OK)
                {
                    // if path contains cyrillic symbols 
                    var cyrillicPath = Regex.Matches(openFileDialog1.FileName, "([А-Яа-яЁё]+)");
                    if (cyrillicPath.Count != 0)
                    {
                        throw new Exception("Path must not contain cyrillic symbols");
                    }
                    TreeBuilder._filePathInput = openFileDialog1.FileName;
                    tbPath.Text = TreeBuilder._filePathInput;
                    btnSelectText.Enabled = true;
                    if (!_treeBuilder.ParseText())
                        return;
                    _treeBuilder.RunWorker(TreeBuilder.Nouns.Split(';')[0], false);
                }
                else
                {
                    tbPath.Text = "";
                    throw new Exception("There is no file selected");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        
    }
}
