﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using Microsoft.Msagl.Drawing;
using System.Text;

namespace Dictionary
{
    class GraphBuilder
    {
        Graph _graph;
        readonly MainForm _UI;
        public static List<Language> _allLanguages;
        public static List<string> _allNouns;
        readonly PausebleBackgroundWorker _worker;

        string _word;
        Language _wordLanguage;

        string _wordWikiText;
        string _languageWikiText;
        string _headerWikiText;

        List<Language> _languages;
        List<Header> _headers;
        List<Link> _links;
        List<Meaning> _meanings;
        List<Hyponim> _hyponims;

        Node _lastSelectedNode;
        bool _firstWord;
        int k;

        public static List<Language> AllLanguages
        {
            get { return _allLanguages; }
        }

        public static List<string> AllNouns
        {
            get { return _allNouns; }
        }

        public GraphBuilder(MainForm UI, string allLanguagesFilePath, string allRussianNounsFilePath, string conceptsFilePath)
        {
            _UI = UI;
            _UI.SetConcepts(GetWordsList(1, conceptsFilePath));
            _allLanguages = GetAllLanguagesList(allLanguagesFilePath);
            _allNouns = GetWordsList(2, allRussianNounsFilePath);
            _worker = new PausebleBackgroundWorker();
            _worker.DoWork += DoWork;
            _worker.RunWorkerCompleted += RunWorkerCompleted;
            _worker.WorkerSupportsCancellation = true;
            _UI.SetGraphMouseClickEvent(Code.ForGraph, GraphMouseClick);
            _wordLanguage = _allLanguages.Find(x => x.Code == "ru");
            _languages = new List<Language>();
            _headers = new List<Header>();
            _links = new List<Link>();
            _meanings = new List<Meaning>();
            _hyponims = new List<Hyponim>();
        }

        public void RunWorker(string word, bool updatingMeaning)
        {
            // Abort current run first
            if (_worker.IsBusy)
            {
                var dialogResult = MessageBox.Show(@"Abort building and start a new tree?",
                    @"Warning", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                if (dialogResult == DialogResult.No)
                {
                    _UI.RedrawGraph(Code.ForGraph, _graph);
                    return;
                }
                _worker.CancelAsync();
                _worker.Resume();
                _UI.AddToLog(Code.ForGraph, "");
            }
            _firstWord = !updatingMeaning;
            _UI.AddToLog(Code.ForGraph, "");
            if (!_worker.IsBusy)
            {
                _worker.RunWorkerAsync(word);
            }
        }

        public void ResumeWorker() => _worker.Resume();

        private void DoWork(object sender, DoWorkEventArgs e)
        {
            AddWord(e.Argument);
            e.Cancel = _worker.CancellationPending;
        }

        private void RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (e.Cancelled)
            {
                if (MainForm.getWordOrConcept)
                    RunWorker(_UI.Concept, false);
                else
                    RunWorker(_UI.Word, false);
            }
            else
            {
                MessageBox.Show(@"Finished building");
            }
            _word = "";
            _wordWikiText = "";
            _languageWikiText = "";
            _headerWikiText = "";
            _languages.Clear();
            _headers.Clear();
            _links.Clear();
            _meanings.Clear();
            _hyponims.Clear();
        }

        private void GraphMouseClick(object sender, MouseEventArgs args)
        {
            if (_UI.GetGraphSelectedObject(Code.ForGraph) is Node)
                _lastSelectedNode = (Node)_UI.GetGraphSelectedObject(Code.ForGraph);
            else
            {
                _lastSelectedNode = null;
                return;
            }
        }

        private void AddWord(object objectWord)
        {
            _word = (string)objectWord;
            // WIKITEXT
            try
            {
                FillWikiText();
            }
            catch (WebException e) // 404
            {
                _UI.AddToLog(Code.ForGraph, $"{_word}: {e.Message}");
                if (_firstWord)
                    return;
                _graph.FindNode(_word).Attr.FillColor = Color.Gray;
                _UI.RedrawGraph(Code.ForGraph, _graph);
                return;
            }
            try
            {
                if (_firstWord)
                {
                    // LANGUAGES
                    FillLanguages("");
                    _UI.RedrawGraph(Code.ForGraph, null);
                    _worker.Wait(); // Приостановить фоновый поток
                    FillHeaders("");
                }
            }
            catch (ArgumentException e) // no languages
            {
                _UI.AddToLog(Code.ForGraph, $"{_word}: {e.Message}");
                if (_firstWord)
                    return;
                _graph.FindNode(_word).Attr.FillColor = Color.Gray;
                _UI.RedrawGraph(Code.ForGraph, _graph);
                return;
            }
            // LINKS
            try
            {
                if (_firstWord)
                {
                    FillMeanings(0, Relation.Hyperonim);
                    _UI.AddToLog(Code.ForGraph, $": {_meanings[0].Text}");
                }
                FillLinks();
            }
            catch (WebException e) // 404
            {
                _UI.AddToLog(Code.ForGraph, $"{_word}: {e.Message}");
                if (_firstWord)
                    return;
                _graph.FindNode(_word).Attr.FillColor = Color.Gray;
                _UI.RedrawGraph(Code.ForGraph, _graph);
                return;
            }
            catch (ArgumentException e) // unknown wikitext type or can't parse known ones
            {
                _UI.AddToLog(Code.ForGraph, $"{_word}: {e.Message}");
                _UI.LanguagesEnabled(false);
                if (!_firstWord)
                {
                    _graph.FindNode(_word).Attr.FillColor = Color.Gray;
                    _UI.RedrawGraph(Code.ForGraph, _graph);
                    return;
                }
            }

            // Add start word
            if (_firstWord)
            {
                //k = 0;
                _graph = new Graph();
                _graph.AddNode(_word);
                _UI.RedrawGraph(Code.ForGraph, _graph);
                _UI.LanguagesEnabled(false);
                _firstWord = false;
                k = 1;
            }
            var node = _graph.FindNode(_word);
            // Stop current building if new graph building started
            if (_worker.CancellationPending)
                return;

            // Add UserData
            if (k == 1 && _meanings.Count != 0)
            {
                node.UserData = _meanings[0].Text;
                k = 0;
            }
            if (_links.Count != 0)
            {
                // Clear node color
                node.Attr.FillColor = Color.White;
                _UI.RedrawGraph(Code.ForGraph, _graph);
                AddLinks();
            }
            else
            {
                node.Attr.FillColor = Color.Gray;
                _UI.RedrawGraph(Code.ForGraph, _graph);
            }
        }

        private void FillWikiText()
        {
            _wordWikiText = Parser.GetWikiText(_word);
            var oldWord = _word;
            if (Parser.PageContainsRedirect(ref _word, ref _wordWikiText))
            {
                _UI.AddToLog(Code.ForGraph, $"{oldWord}: redirect -> {_word}");
                // Change early added node
                if (!_firstWord)
                {
                    var node = _graph.FindNode(oldWord);
                    var sources = new List<string>();
                    foreach (var inEdge in node.InEdges.ToList())
                    {
                        sources.Add(inEdge.Source);
                        _graph.RemoveEdge(inEdge);
                    }
                    _graph.NodeMap.Remove(node.Id);
                    _graph.GeometryGraph.Nodes.Remove(node.GeometryObject as Microsoft.Msagl.Core.Layout.Node);
                    _graph.AddNode(_word);
                    foreach (var source in sources)
                    {
                        _graph.AddEdge(source, _word);
                    }
                    _UI.RedrawGraph(Code.ForGraph, _graph);
                }
            }
            Parser.RemoveHtmlCode(ref _wordWikiText);
            _UI.AddToLog(Code.ForGraph, $"{_word}: choose meaning");
        }

        private void FillLanguages(string wikiText)
        {
            _languages.Clear();
            bool flag = wikiText == "" ? true : false;
            wikiText = flag ? _wordWikiText : wikiText;
            var languagesCodes = Parser.GetLanguageCodes(false, wikiText);
            if (!flag && languagesCodes == null)
                throw new ArgumentException();
            foreach (var languageCode in languagesCodes)
            {
                //Search for language with the same code in all languages list and add it to word's languages list
                var language = _allLanguages.Find(lang => lang.Code == languageCode);
                if (language == null)
                    language = new Language(languageCode + " [unknown]", languageCode);
                _languages.Add(language);
            }
            if (!flag && !_languages.Exists(lang => lang == _wordLanguage))
                throw new ArgumentException();
            if (flag)
                _UI.SetLanguages(_languages);
        }

        public void FillHeaders(string wikiText)
        {
            _headers.Clear();
            wikiText = wikiText == "" ? _wordWikiText : wikiText;
            int selectedLanguageIndex = _languages.IndexOf(_wordLanguage);
            _languageWikiText = Parser.GetSelectedLanguageWikiText(wikiText, _languages, selectedLanguageIndex);
            _headers = Parser.GetHeaders(_languageWikiText);
        }

        public void Show(int count, MatchCollection links)
        {
            if (count % 50 == 0 || count == links.Count)
                _UI.AddToLog(Code.ForGraph, $"--viewed {count} links");
        }

        public void FillLinks()
        {
            _links.Clear();
            var nameLinks = new List<string>();
            var lhcont = false;
            long lhcontNum = -1;
            do
            {
                // get whole list of titles links here
                var lhwikiText = Parser.GetWikiLinks(lhcont, lhcontNum, _word);
                var lhcontinue = Regex.Matches(lhwikiText, "lhcontinue=\"([0-9]*)\"");
                // if links exist (max = 500 per one time)
                if (lhcontinue.Count > 0) 
                {
                    lhcont = true;
                    lhcontNum = long.Parse(lhcontinue[0].Groups[1].Value);
                }
                else
                {
                    lhcont = false;
                    lhcontNum = -1;
                }

                var allLinks = Regex.Matches(lhwikiText, "lh ns=\"[0-9]*\" title=\"([^\"]+)");

                if (allLinks.Count == 0)
                    throw new ArgumentException("wikitext page does not contain any pages that link here");

                var str = lhcont ? "+ ..." : "";
                _UI.AddToLog(Code.ForGraph, $"--found {allLinks.Count} links here {str}");
                
                int count = 1;
                // check each link in list
                foreach (Match link in allLinks) //if (allLinks.Count > 0)
                {
                    // взять первую ссылку, и найти в ее гиперонимах название запроса
                    var curName = link.Groups[1].Value;
                    
                    // проверим есть ли такая статья в категории Русские существительные
                    if (!_allNouns.Contains(_word))
                    {
                        _UI.AddToLog(Code.ForGraph, $"there is no word: {_word}");
                        Show(count, allLinks); count++; continue;
                    }

                    string wikiText;
                    try
                    {
                        wikiText = Parser.GetWikiText(curName);
                        bool smt = Parser.PageContainsRedirect(ref curName, ref wikiText);
                    }
                    catch (WebException) // ссылки нет
                    {
                        Show(count, allLinks); count++; continue;
                    }

                    try
                    {
                        FillLanguages(wikiText);
                    }
                    catch (ArgumentException)
                    {
                        Show(count, allLinks); count++; continue;
                    }

                    FillHeaders(wikiText);

                    int i = 0;
                    do
                    {
                        // check: word is noun or not
                        var checkText = Parser.GetSelectedHeaderWikiText(_languageWikiText, _headers, i);
                        checkText = checkText.Length == 0 ? _languageWikiText : checkText;
                        var check = Regex.Matches(checkText, "сущ.*");
                        if (check.Count == 0)
                        {
                            i++; continue;
                        }

                        try
                        {
                            FillMeanings(i, Relation.Hyperonim);
                            FillMeanings(i, Relation.Hyponim);
                        }
                        catch (ArgumentException)
                        {
                            i++; continue;
                        }
                        if (_meanings == null)
                        {
                            i++; continue;
                        }
                        
                        for (int j = 0; j < _meanings.Count; j++)
                        {
                            var hyper = _meanings[j].Hyperonims;
                            if (hyper == null) //если не нашли
                            {
                                continue;
                            }
                            if (hyper.Exists(x => x == _word))
                            {
                                nameLinks.Add(curName);
                                _UI.AddToLog(Code.ForGraph, $"-- {curName}");
                                _UI.AddToLog(Code.ForGraph, $" : {_meanings[j].Text}");

                                var hypon = _hyponims[j].Hyponims;
                                if (hypon == null) //если не нашли
                                {
                                    continue;
                                }                                
                                foreach (var item in hypon)
                                {
                                    if (item != "" && !item.Contains("}}") && !nameLinks.Exists(x => x == item))
                                    {
                                        nameLinks.Add(item);
                                        _UI.AddToLog(Code.ForGraph, $"-- {item}");
                                        _UI.AddToLog(Code.ForGraph, $" : {_hyponims[j].Text}");
                                    }
                                }
                            }
                        }
                        i++;
                    }
                    while (i < _headers.Count);
                    Show(count, allLinks);
                    count++;
                }
            }
            while (lhcont);
            _links.Add(new Link(_word, nameLinks)); //name_links as hyperonims
        }

        public void SetHyperonimOrHyponim(Relation rel, List<Meaning> temp)
        {
            if (rel == Relation.Hyperonim)
            {
                for (int i = 0; i < temp.Count; i++)
                    _meanings.Add(new Meaning(temp[i].Text, temp[i].Hyperonims));
            }
            else
            {
                for (int i = 0; i < temp.Count; i++)
                    _hyponims.Add(new Hyponim(temp[i].Text, temp[i].Hyperonims));
            }
        }

        public void FillMeanings(int i, Relation rel)
        {
            if (rel == Relation.Hyperonim)
                _meanings.Clear();
            else
                _hyponims.Clear();
            List<Meaning> temp = new List<Meaning>();
            _headerWikiText = Parser.GetSelectedHeaderWikiText(_languageWikiText,
                _headers, i);
            // If there are no headers, use language wikitext
            var wikiText = _headerWikiText.Length == 0 ? _languageWikiText : _headerWikiText;
            var type = Parser.DetectWikiTextType(wikiText);
            switch (type)
            {
                case Parser.WikiTextType.First:
                    temp = Parser.GetMeaningsListT1(rel, wikiText);
                    SetHyperonimOrHyponim(rel, temp);
                    break;
                case Parser.WikiTextType.Second:
                    temp = Parser.GetMeaningsListT2(rel, wikiText);
                    SetHyperonimOrHyponim(rel, temp);
                    break;
                case Parser.WikiTextType.Third:
                    temp = Parser.GetMeaningsListT3(rel, wikiText);
                    SetHyperonimOrHyponim(rel, temp);
                    break;
                case Parser.WikiTextType.Unknown:
                    if (rel == Relation.Hyperonim)
                        _meanings.Clear();
                    else
                        _hyponims.Clear();
                    temp.Clear();
                    throw new ArgumentException(_word + ": unknown wikitext type");
            }
            temp.Clear();
        }

        private void AddLinks()
        {
            var parentWord = _word;
            _UI.AddToLog(Code.ForGraph, $"{parentWord}: adding link here");
            foreach (var hyperonim in _links[0].Hyperonims)
            {
                if (hyperonim.Length == 0)
                    continue;
                if (_graph.FindNode(hyperonim) != null)
                {
                    if (!EdgeExists(_graph, parentWord, hyperonim))
                    {
                        _graph.AddEdge(parentWord, hyperonim);
                        _UI.RedrawGraph(Code.ForGraph, _graph);
                    }
                    continue;
                }
                _graph.AddNode(hyperonim);
                _graph.AddEdge(parentWord, hyperonim);
                _UI.RedrawGraph(Code.ForGraph, _graph);
                if (_worker.CancellationPending)
                {
                    return;
                }
                AddWord(hyperonim);
            }
            _UI.AddToLog(Code.ForGraph, $"{parentWord}: added all link here");
        }
        
        /// <summary>
        /// Checks if edge (both directions) exists in graph
        /// </summary>
        /// <param name="graph">Graph reference</param>
        /// <param name="first">First node id</param>
        /// <param name="second">Second node id</param>
        /// <returns></returns>
        private bool EdgeExists(Graph graph, string first, string second)
        {
            return graph.Edges.Any(edge => edge.Source == first && edge.Target == second ||
            edge.Source == second && edge.Target == first);
        }

        //Initializes list of all languages from special file
        public static List<Language> GetAllLanguagesList(string fileName)
        {
            var languages = new List<Language>();
            using (var sr = new StreamReader(fileName))
            {
                while (!sr.EndOfStream)
                {
                    var line = sr.ReadLine();
                    if (line == null) continue;
                    var split = line.Split('\t');
                    languages.Add(new Language(split[1], split[0]));
                }
            }
            return languages;
        }

        //Initializes list of: code == 1 then concepts, code == 2 all russian nouns from special file,
        public static List<string> GetWordsList(int code, string fileName)
        {
            var temp = new List<string>();
            if (code == 1)
                temp.Add("");
            using (var sr = new StreamReader(fileName, Encoding.Default))
            {
                while (!sr.EndOfStream)
                {
                    temp.Add(sr.ReadLine());
                }
            }
            return temp;
        }        

    }
}