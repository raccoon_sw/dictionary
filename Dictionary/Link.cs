﻿using System.Collections.Generic;

namespace Dictionary
{
    public class Link
    {
        public string Text { get; set; }
        public List<string> Hyperonims { get; set; }
        public Link(string text, List<string> hyperonims)
        {
            Text = text;
            Hyperonims = hyperonims;
        }
    }
}
