﻿namespace Dictionary
{
    public class Header
    {
        public string Text { get; }
        public string Template { get; }
        public Header(string text, string template)
        {
            Text = text;
            Template = template;
        }
    }
}

