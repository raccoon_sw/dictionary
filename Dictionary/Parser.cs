﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;
using MySql.Data.MySqlClient;
using System.Windows.Forms;

namespace Dictionary
{
    static class Parser
    {
        static string conString = Properties.Settings.Default.StringConnection;

        private static string change(string str)
        {
            if (str.Contains('\''))
            {
                string[] arr = str.Split('\'');
                str = "";
                for (int i = 0; i < arr.Length - 1; i++)
                    str = arr[i] + "\\'" + arr[i + 1];
            }
            return str;
        }

        // get wikitext for word
        public static string GetWikiText(string word)
        {
            word = word.Replace(" ", "_");
            var wikiText = "";
            MySqlConnection connection = null;
            try
            {
                using (connection = new MySqlConnection(conString))
                {
                    connection.Open();
                    var strRevTextId = "";
                    // find in database all ids of page which has title = word
                    using (MySqlCommand getRevTextId = new MySqlCommand(@"select rev_text_id from revision left join page on " +
                    " page_latest = rev_id where page_title = '" + change(word) + "'; ", connection))
                    {
                        MySqlDataReader readRevTextId = getRevTextId.ExecuteReader();
                        // if database hasn't any page with such title
                        if (!readRevTextId.HasRows)
                        {
                            return wikiText;
                        }
                        strRevTextId = "(";
                        while (readRevTextId.Read())
                        {
                            strRevTextId += readRevTextId[0] + ", ";
                        }
                        strRevTextId = strRevTextId.Trim();
                        strRevTextId = strRevTextId.Remove(strRevTextId.Length - 1) + ")";
                    }

                    // find in database all texts of page which has title = word
                    using (MySqlCommand getPageText = new MySqlCommand(@"select convert(old_text using utf8) from text where " +
                        " old_id in " + strRevTextId + ";", connection))
                    {
                        MySqlDataReader readPageText = getPageText.ExecuteReader();
                        // if database hasn't any page with such title
                        if (!readPageText.HasRows)
                        {
                            return wikiText;
                        }

                        // get page (all information)
                        while (readPageText.Read())
                        {
                            wikiText = readPageText[0].ToString();
                            var check = Regex.Matches(wikiText, "= {{-ru-}} =");
                            //var check = Regex.Matches(wikiText, "{{.*");
                            if (check.Count == 0)
                                continue;
                            else
                                break;
                        }
                    }
                    connection.Close();
                }
            }
            catch
            {
                MessageBox.Show("Connection error!");
                return wikiText;
            }
            return wikiText;
        }

        public static string GetWikiLinks(bool lhcont, long lhcont_num, string word)
        {
            // т.к. бд неполная, в ней нет таблицы pagelinks
            HttpWebRequest request;
            if (!lhcont)
                request = (HttpWebRequest)WebRequest.Create(
                    "https://ru.wiktionary.org/w/api.php?action=query&prop=linkshere&lhlimit=max&lhprop=title&format=xml&titles=" + word); //&lhprop=pageid|title
            else
                request = (HttpWebRequest)WebRequest.Create(
                    "https://ru.wiktionary.org/w/api.php?action=query&prop=linkshere&lhlimit=max&lhprop=title&format=xml&lhcontinue="
                    + lhcont_num + "&titles=" + word);

            var response = (HttpWebResponse)request.GetResponse();
            var data = response.GetResponseStream();
            string wikiText;
            // get all links here for this page
            using (var reader = new StreamReader(data)) 
            {
                wikiText = reader.ReadToEnd();
            }
            return wikiText;
        }

        //Gets list of language codes from word's wikitext
        public static List<string> GetLanguageCodes(bool for_link, string wikiText)
        {
            var langCodesList = new List<string>();
            var matches = Regex.Matches(wikiText, "= {{-([^{}]+)-}} =");
            if (matches.Count > 0)
            {
                foreach (Match match in matches)
                {
                    var langCode = match.Groups[1].Value;
                    langCodesList.Add(langCode);
                }
            }
            else
            {
                if (for_link)
                    langCodesList = null;
                else
                    throw new ArgumentException("wikitext page does not contain any language codes");
            }
            return langCodesList;
        }

        //Gets list of headers from word's wikitext
        public static List<Header> GetHeaders(string wikiText)
        {
            var headersList = new List<Header>();
            var matches = Regex.Matches(wikiText, "== {{з(аголовок)?[|]([^{}]+)}} ==");
            foreach (Match match in matches)
            {
                var header = new Header(match.Groups[2].Value, match.Value);
                headersList.Add(header);
            }
            return headersList;
        }
        
        // Returns true and changes word and wikitext to redirect target if page contains redirect
        public static bool PageContainsRedirect(ref string word, ref string wikiText)
        {
            if (!wikiText.Contains("#REDIRECT")
                && !wikiText.Contains("#Redirect")
                && !wikiText.Contains("#перенаправление")) return false;
            var match = Regex.Match(wikiText, @"\[\[(.+)]]");
            if (!match.Success) throw new WebException(word + ": unknown redirect pattern");
            word = match.Groups[1].Value;
            wikiText = GetWikiText(word);
            return true;
        }

        public static string GetSelectedLanguageWikiText(string wikiText, List<Language> languagesList, int selectedLanguageIndex)
        {
            //Cutting of specific part of wikitext for chosen language
            var selectedLanguage = languagesList[selectedLanguageIndex];
            //Wikitext language code format: = {{-code-}} =
            var template = "= {{-" + selectedLanguage.Code + "-}} =";
            //Start position for cutting is chosen language code's location in wikitext
            var startPos = wikiText.IndexOf(template, StringComparison.Ordinal) + template.Length;
            //If chosen language is the last one in the list 
            //then end position for cutting is wikitext's end
            int endPos;
            if (selectedLanguageIndex == languagesList.Count - 1)
            {
                endPos = wikiText.Length;
            }
            //else it's just a location of the next language code
            else
            {
                var nextLanguage = languagesList[selectedLanguageIndex + 1];
                template = "= {{-" + nextLanguage.Code + "-}} =";
                endPos = wikiText.IndexOf(template, startPos, StringComparison.Ordinal);
            }
            return wikiText.Substring(startPos, endPos - startPos);
        }

        // Returns empty string, if there are no headers
        public static string GetSelectedHeaderWikiText(string wikiText, List<Header> headersList, int selectedHeaderIndex)
        {
            if (headersList.Count == 0)
                return "";
            //Cutting of specific part of wikitext for chosen header
            var selectedHeader = headersList[selectedHeaderIndex];
            var template = selectedHeader.Template;
            //Start position for cutting is chosen header code's location in wikitext
            var startPos = wikiText.IndexOf(template, StringComparison.Ordinal) + template.Length;
            //If chosen language is the last one in the list 
            //then end position for cutting is wikitext's end
            int endPos;
            if (selectedHeaderIndex == headersList.Count - 1)
            {
                endPos = wikiText.Length;
            }
            //else it's just a location of the next header code
            else
            {
                var nextHeader = headersList[selectedHeaderIndex + 1];
                template = nextHeader.Template;
                endPos = wikiText.IndexOf(template, startPos, StringComparison.Ordinal);
            }            
            return wikiText.Substring(startPos, endPos - startPos);
        }

        public enum WikiTextType
        {
            First,
            Second,
            Third,
            Unknown
        }

        public static string GetMeaningsListForLinks(Relation rel, string _word, string cur_name, string wikiText)
        {
            //Get hyperonimes groups list (grouped by meanings)
            var hypers = GetHypersGroupsListT1(rel, wikiText);
            if (hypers == null) //если не нашли
                throw new ArgumentException();

            if (hypers.Exists(hyper => hyper.Contains("[[" + _word + "]]")))
                return cur_name;
            else
                throw new ArgumentException();
        }


        //Detects wikitext design pattern
        public static WikiTextType DetectWikiTextType(string wikiText)
        {
            var match = Regex.Match(wikiText, "===[ ]?Семантические свойства[ ]?===");
            if (!match.Success)
                return WikiTextType.Unknown;
            if (wikiText.Contains("{{списки семантических связей}}"))
                return WikiTextType.Third;
            match = Regex.Match(wikiText, "====[ ]?Значение[ ]?====");
            return match.Success ? WikiTextType.First : WikiTextType.Second;
        }

        //Extracts word's meanings from wikitext
        //Returns list of them (First wikitext type)
        public static List<Meaning> GetMeaningsListT1(Relation rel, string wikiText)
        {
            var meaningsList = new List<Meaning>();
            //Pick out meanings block from wikitext
            var match = Regex.Match(wikiText, "====[ ]?Значение[ ]?====");
            var startPos = wikiText.IndexOf(match.Value, StringComparison.Ordinal) + match.Value.Length;
            var endPos = wikiText.IndexOf("\n===", startPos, StringComparison.Ordinal);

            if (endPos == -1) throw new ArgumentException("(First type) failed to parse wikitext: incomplete or incorrect filling");

            var meaningsText = wikiText.Substring(startPos, endPos - startPos);

            //Remove {{прото|...}} template
            meaningsText = Regex.Replace(meaningsText, @"{{прото[^\n]+\n", "");

            var meanings = meaningsText.Split(new[] { "\n#" }, StringSplitOptions.RemoveEmptyEntries).ToList();
            if (string.IsNullOrWhiteSpace(meanings[0]))
                meanings.RemoveAt(0);

            //Get hyperonimes groups list (grouped by meanings)
            var hypersGroups = GetHypersGroupsListT1(rel, wikiText);

            //If meanings count and hyperonimes groups count are not equal then add some empty groups
            if (hypersGroups.Count < meanings.Count)
            {
                var difference = meanings.Count - hypersGroups.Count;
                for (var i = 0; i < difference; i++)
                    hypersGroups.Add("");
            }

            for (var i = 0; i < meanings.Count; i++)
            {
                var meaning = meanings[i];

                if (string.IsNullOrWhiteSpace(meaning))
                    continue;

                HandleTemplates(ref meaning);

                //Extract hyperonimes from hyperonimes group according to current meaning
                var hyperonims = hypersGroups[i].Split(',', ';');
                for (var j = 0; j < hyperonims.Length; j++)
                {
                    HandleReferenceTemplate(ref hyperonims[j]);     //[[...]] or [[...|...]]
                    hyperonims[j] = hyperonims[j].Trim();
                    CheckHyperonimForEmptyness(ref hyperonims[j]);  //check for emptyness
                }
                meaningsList.Add(new Meaning(meaning, hyperonims.ToList()));
            }
            return meaningsList;
        }

        //Extracts word's hyperonims from its wikitext
        //returns list of their groups (grouped by meanings)
        public static List<string> GetHypersGroupsListT1(Relation rel, string wikiText)
        {
            var relationType = rel == Relation.Hyperonim ? "==== Гиперонимы ====\n" : "==== Гипонимы ====\n";
            var forPrint = rel == Relation.Hyperonim ? "hyperonimes" : "hyponimes";
            var hypersGroupsList = new List<string>();
            //It's possible that there is no hyperonims part. If so, just return empty groups list
            if (!wikiText.Contains(relationType))
                return hypersGroupsList;
            var startPos = wikiText.IndexOf(relationType, StringComparison.Ordinal) + relationType.Length - 1;
            var endPos = wikiText.IndexOf("\n===", startPos, StringComparison.Ordinal);

            if (endPos == -1) throw new ArgumentException("(First type) failed to parse " + forPrint + "' wikitext: incomplete or incorrect filling");

            var hypersStr = wikiText.Substring(startPos, endPos - startPos);
            var hypersGroups = hypersStr.Split(new[] { "\n#" }, StringSplitOptions.RemoveEmptyEntries);
            for (var i = 0; i < hypersGroups.Length; i++)
            {
                HandleCommonTemplates(ref hypersGroups[i]);
                hypersGroupsList.Add(hypersGroups[i]);
            }
            return hypersGroupsList;
        }

        //Extracts word's meanings (include hyperonims) from its wikitext
        //Returns list of them (Second wikitext type)
        public static List<Meaning> GetMeaningsListT2(Relation rel, string wikiText)
        {
            var relationType = rel == Relation.Hyperonim ? "[|]гиперонимы[ ]*=[ ]*(.+)\n" : "[|]гипонимы[ ]*=[ ]*(.+)\n";
            var meaningsList = new List<Meaning>();
            var match = Regex.Match(wikiText, "===[ ]?Семантические свойства[ ]?===");
            var startPos = wikiText.IndexOf(match.Value, StringComparison.Ordinal) + match.Value.Length;
            var endPos = wikiText.IndexOf("\n=== ", startPos, StringComparison.Ordinal);

            if (endPos == -1) throw new ArgumentException("(Second type) failed to parse wikitext: incomplete or incorrect filling");

            var semanticsText = wikiText.Substring(startPos, endPos - startPos);
            var semanticsGroups = semanticsText.Split(new[] { "# {{значение" }, StringSplitOptions.RemoveEmptyEntries);
            foreach (var semanticsGroup in semanticsGroups)
            {
                //Get meaning
                match = Regex.Match(semanticsGroup, "[|]определение[ ]*=[ ]*(.+)\n");
                if (!match.Success) continue;
                var meaning = match.Groups[1].Value;

                HandleTemplates(ref meaning);

                //Get hyperonims/hyponims
                match = Regex.Match(semanticsGroup, relationType);
                var hypersStr = match.Groups[1].Value;
                var hyperonims = hypersStr.Split(',', ';');
                for (var j = 0; j < hyperonims.Length; j++)
                {
                    HandleReferenceTemplate(ref hyperonims[j]);     //[[...]] or [[...|...]]
                    hyperonims[j] = hyperonims[j].Trim();
                    CheckHyperonimForEmptyness(ref hyperonims[j]);  //check for emptyness
                }
                meaningsList.Add(new Meaning(meaning, hyperonims.ToList()));
            }
            return meaningsList;
        }

        //Extracts word's meanings (include hyperonims) from its wikitext
        //Returns list of them (Third wikitext type)
        public static List<Meaning> GetMeaningsListT3(Relation rel, string wikiText)
        {
            var relationType = rel == Relation.Hyperonim ? "|гиперонимы=" : "|гипонимы=";
            var meaningsList = new List<Meaning>();
            var match = Regex.Match(wikiText, "====[ ]?Значение[ ]?====");
            var startPos = wikiText.IndexOf(match.Value, StringComparison.Ordinal) + match.Value.Length;
            var endPos = wikiText.IndexOf("{{списки семантических связей}}\n", startPos, StringComparison.Ordinal);

            if (endPos == -1) throw new ArgumentException("(Third type) failed to parse wikitext: incomplete or incorrect filling");

            var meaningsText = wikiText.Substring(startPos, endPos - startPos);

            //Remove {{прото|...}} template
            meaningsText = Regex.Replace(meaningsText, @"{{прото[^\n]+\n", "");

            var meanings = meaningsText.Split(new[] { "\n#" }, StringSplitOptions.RemoveEmptyEntries);
            for (var i = 0; i < meanings.Length; i++)
            {
                if (string.IsNullOrWhiteSpace(meanings[i]))
                    continue;

                //Extract semantics part
                match = Regex.Match(meanings[i], "{{семантика(.+)}}", RegexOptions.Singleline);
                string semantics;
                if (match.Success)
                {
                    semantics = match.Groups[1].Value;
                    meanings[i] = meanings[i].Replace(match.Groups[0].Value, "");
                }
                //If there is no semantics part in wikitext (somebody forgot to add), add it
                else
                {
                    semantics = "|синонимы=|антонимы=|гиперонимы=|гипонимы=";
                }

                HandleTemplates(ref meanings[i]);

                //Get hyperonims/hyponims
                var hypersStr = "";
                if (semantics.Contains(relationType))
                {
                    startPos = semantics.IndexOf(relationType, StringComparison.Ordinal) + relationType.Length;
                    endPos = semantics.IndexOf("|", startPos, StringComparison.Ordinal);
                    hypersStr = semantics.Substring(startPos, endPos - startPos);
                }
                var hyperonims = hypersStr.Split(',', ';');
                for (var j = 0; j < hyperonims.Length; j++)
                {
                    HandleReferenceTemplate(ref hyperonims[j]);     //[[...]] or [[...|...]]
                    hyperonims[j] = hyperonims[j].Trim();
                    CheckHyperonimForEmptyness(ref hyperonims[j]);  //check for emptyness
                }
                meaningsList.Add(new Meaning(meanings[i], hyperonims.ToList()));
            }

            return meaningsList;
        }

        private static void CheckHyperonimForEmptyness(ref string hyperonim)
        {
            if (new[] { "-", "—", "{{-}}", "?", "&nbsp", "&#160" }.Contains(hyperonim))
                hyperonim = "";
        }

        private static void HandleTemplates(ref string meaning)
        {
            HandleReferenceTemplate(ref meaning);           // (ссылка)
            HandleCommonTemplates(ref meaning);             // (общие)
            HandleVerbTemplate(ref meaning);                // (ГЛАГОЛ)
            HandleAdverbTemplate(ref meaning);              // (НАРЕЧИЕ)
            HandleNounTemplate(ref meaning);                // (СУЩЕСТВИТЕЛЬНОЕ)
            HandleAdjectiveTemplate(ref meaning);           // (ПРИЛАГАТЕЛЬНОЕ)
            HandleComparativeTemplate(ref meaning);         // (СРАВНИТЕЛЬНАЯ СТЕПЕНЬ)
            HandleSuperlativeTemplate(ref meaning);         // (ПРЕВОСХОДНАЯ СТЕПЕНЬ)
            HandleParticipleTemplate(ref meaning);          // (ПРИЧАСТИЕ)
            HandleAdverbialParticipleTemplate(ref meaning); // (ДЕЕПРИЧАСТИЕ)
            HandlePassiveVoiceTemplate(ref meaning);        // (СТРАДАТЕЛЬНЫЙ ЗАЛОГ)
            HandlePerformTemplate(ref meaning);             // (СОВЕРШИТЬ)
            HandleTheSameTemplate(ref meaning);             // (то же что и)
            HandlePometaTemplate(ref meaning);              // (помета)
            RemovePrefixes(ref meaning);                    // (префиксы)
            RemovePrefixes(ref meaning);                    // (префиксы) x2 (КОСТЫЫЫЫЫЫЛЬ)
            meaning = meaning.Trim(' ', ',', ':', ';', '\n', '\'');
            meaning = meaning.Replace("()", "");
        }

        //Handles reference template ([[...]] or [[...|...]])
        private static void HandleReferenceTemplate(ref string wikiText)
        {
            wikiText = Regex.Replace(wikiText, "[[]{2}[^|[\\]]+[|]", "");
            wikiText = wikiText.Replace("[[", "").Replace("]]", "");
        }

        //Handles {{действие|...}} template (ГЛАГОЛ)
        private static void HandleVerbTemplate(ref string wikiText)
        {
            var match = Regex.Match(wikiText, "{{действие([^{}]+)}}");
            if (!match.Success) return;
            var paramsStr = match.Groups[1].Value;
            var lang = Regex.Match(paramsStr, "([|]lang=[^|{}]+)[|]?");
            if (lang.Success)
                paramsStr = paramsStr.Replace(lang.Groups[1].Value, "");
            var parameters = paramsStr.Split(new[] { "|" }, StringSplitOptions.RemoveEmptyEntries);
            string resultStr;
            if (paramsStr.Contains("состояние=1"))
                resultStr = "действие или состояние по значению гл. " + parameters[0];
            else
                resultStr = "действие по значению гл. " + parameters[0];
            if (parameters.Length > 1)
                resultStr += "; " + parameters[1];
            wikiText = wikiText.Replace(match.Value, resultStr);
        }

        //Handles {{наречие|...}} template (НАРЕЧИЕ)
        private static void HandleAdverbTemplate(ref string wikiText)
        {
            var match = Regex.Match(wikiText, "{{наречие[|]([^{}]+)}}");
            if (!match.Success) return;
            var parameters = match.Groups[1].Value.Split('|');
            var resultStr = "наречие к прилагательному " + parameters[0];
            if (parameters.Length > 1)
                resultStr += "; " + parameters[1];
            wikiText = wikiText.Replace(match.Value, resultStr);
        }

        //Handles {{свойство|...}} template (СУЩЕСТВИТЕЛЬНОЕ)
        private static void HandleNounTemplate(ref string wikiText)
        {
            var match = Regex.Match(wikiText, "{{свойство([^{}]+)}}");
            if (!match.Success) return;
            var paramsStr = match.Groups[1].Value;
            var lang = Regex.Match(paramsStr, "([|]lang=[^|{}]+)[|]?");
            if (lang.Success)
                paramsStr = paramsStr.Replace(lang.Groups[1].Value, "");
            var containsState = false;
            if (paramsStr.Contains("|состояние=1"))
            {
                containsState = true;
                paramsStr = paramsStr.Replace("|состояние=1", "");
            }
            var resultStr = "свойство ";
            if (containsState)
                resultStr += "или состояние ";
            resultStr += "по значению прилагательного ";
            var parameters = paramsStr.Split(new[] { "|" }, StringSplitOptions.RemoveEmptyEntries);
            resultStr += parameters[0];
            if (parameters.Length > 1)
                resultStr += "; " + parameters[1];
            wikiText = wikiText.Replace(match.Value, resultStr);
        }

        //Handles {{соотн.|...}} template (ПРИЛАГАТЕЛЬНОЕ)
        private static void HandleAdjectiveTemplate(ref string wikiText)
        {
            var match = Regex.Match(wikiText, "{{соотн[\\.]([^{}]+)}}");
            if (!match.Success) return;
            var paramsStr = match.Groups[1].Value;
            var lang = Regex.Match(paramsStr, "([|]lang=[^|{}]+)[|]?");
            if (lang.Success)
                paramsStr = paramsStr.Replace(lang.Groups[1].Value, "");
            var property = Regex.Match(paramsStr, "[|]свойств=([^|]+)[|]?"); //свойств=...
            if (property.Success)
                paramsStr = paramsStr.Replace(property.Value, "");
            //ToDo var motive = 
            var words = paramsStr.Split(new[] { "|" }, StringSplitOptions.RemoveEmptyEntries);
            var resultStr = "связанный, соотносящийся по значению с существительным" + (words.Length > 1 ? "и " : " ");
            foreach (var word in words)
            {
                resultStr += word + ", ";
            }
            resultStr = resultStr.Remove(resultStr.Length - 2);
            if (property.Success)
            {
                resultStr += "; свойственный, характерный для ";
                if (property.Groups[1].Value == "1")
                {
                    resultStr += words.Length > 1 ? "них" : "него";
                }
                else
                {
                    resultStr += property.Groups[1].Value;
                }
            }
            wikiText = wikiText.Replace(match.Value, resultStr);
        }

        //Handles {{сравн.|...}} template (СРАВНИТЕЛЬНАЯ СТЕПЕНЬ)
        private static void HandleComparativeTemplate(ref string wikiText)
        {
            var match = Regex.Match(wikiText, "{{сравн[.]([^{}]+)}}");
            if (!match.Success) return;
            var paramsStr = match.Groups[1].Value;
            var lang = Regex.Match(paramsStr, "([|]lang=[^|{}]+)[|]?");
            if (lang.Success)
                paramsStr = paramsStr.Replace(lang.Groups[1].Value, "");
            var parameters = paramsStr.Split(new[] { "|" }, StringSplitOptions.RemoveEmptyEntries);
            var resultStr = "сравнительная степень к ";
            if (parameters.Length > 1 && parameters[1] == "к=нареч")
                resultStr += "наречию " + parameters[0];
            else
                resultStr += "прилагательному " + parameters[0];
            wikiText = wikiText.Replace(match.Value, resultStr);
        }

        //Handles {{превосх.|...}} template (ПРЕВОСХОДНАЯ СТЕПЕНЬ)
        private static void HandleSuperlativeTemplate(ref string wikiText)
        {
            var match = Regex.Match(wikiText, "{{превосх[.]([^{}]+)}}");
            if (!match.Success) return;
            var paramsStr = match.Groups[1].Value;
            var lang = Regex.Match(paramsStr, "([|]lang=[^|{}]+)[|]?");
            if (lang.Success)
                paramsStr = paramsStr.Replace(lang.Groups[1].Value, "");
            var parameters = paramsStr.Split(new[] { "|" }, StringSplitOptions.RemoveEmptyEntries);
            var resultStr = "превосходная степень к ";
            if (parameters.Length > 1 && parameters[1] == "к=нареч")
                resultStr += "наречию " + parameters[0];
            else
                resultStr += "прилагательному " + parameters[0];
            wikiText = wikiText.Replace(match.Value, resultStr);
        }

        //Handles {{прич.|...}} template (ПРИЧАСТИЕ)
        private static void HandleParticipleTemplate(ref string wikiText)
        {
            var match = Regex.Match(wikiText, "{{прич[.][|]([^{}]+)}}");
            if (!match.Success) return;
            var parameters = match.Groups[1].Value.Split('|');
            var time = "";
            if (parameters.Length > 1)
            {
                time = parameters[1] == "наст" ? "настоящего времени " : "прошедшего времени ";
            }
            var voice = parameters.Length > 2 && parameters[2] == "страд" ? "страдательное " : "действительное ";
            var resultStr = voice + "причастие " + time + "от " + parameters[0];
            wikiText = wikiText.Replace(match.Value, resultStr);
        }

        //Handles {{дееприч.|...}} template (ДЕЕПРИЧАСТИЕ)
        private static void HandleAdverbialParticipleTemplate(ref string wikiText)
        {
            var match = Regex.Match(wikiText, "{{дееприч[.][|]([^{}]+)}}");
            if (!match.Success) return;
            var parameters = match.Groups[1].Value.Split('|');
            var resultStr = "деепричастие";
            if (parameters.Length > 1 && !string.IsNullOrEmpty(parameters[1]))
                resultStr += " (" + parameters[1] + ". залог)";
            resultStr += " от " + parameters[0];
            wikiText = wikiText.Replace(match.Value, resultStr);
        }

        //Handles {{страд.|...}} template (СТРАДАТЕЛЬНЫЙ ЗАЛОГ)
        private static void HandlePassiveVoiceTemplate(ref string wikiText)
        {
            var match = Regex.Match(wikiText, "{{страд[.][|]([^{}]+)}}");
            if (!match.Success) return;
            wikiText = wikiText.Replace(match.Value, "страдательный залог к " + match.Groups[1].Value);
        }

        //Handles {{совершить|...}} template (СОВЕРШИТЬ)
        private static void HandlePerformTemplate(ref string wikiText)
        {
            var match = Regex.Match(wikiText, "{{совершить[|]([^{}]+)}}");
            if (!match.Success) return;
            wikiText = wikiText.Replace(match.Value, "совершить действие, выраженное глаголом " + match.Groups[1].Value + "; провести некоторое время, совершая такое действие");
        }

        //Handles {{=|word}} template
        private static void HandleTheSameTemplate(ref string wikiText)
        {
            var match = Regex.Match(wikiText, "{{=[|]([^{}]+)}}");
            if (!match.Success) return;
            var replaceStr = "";
            if (match.Groups[1].Value.Contains('|'))
            {
                var words = match.Groups[1].Value.Split('|');
                replaceStr = words.Aggregate(replaceStr, (current, word) => current + word + ", ");
                replaceStr = replaceStr.Remove(replaceStr.Length - 2, 2);
            }
            else
            {
                replaceStr = match.Groups[1].Value;
            }
            wikiText = wikiText.Replace(match.Value, "то же, что " + replaceStr);
        }

        //Handles {{помета|...}} template
        private static void HandlePometaTemplate(ref string wikiText)
        {
            var matches = Regex.Matches(wikiText, "{{помета[|]([^{}]+)}}");
            if (matches.Count == 0) return;
            foreach (Match match in matches)
            {
                wikiText = wikiText.Replace(match.Value, match.Groups[1].Value);
            }
        }
        
        //Handles other common templates
        private static void HandleCommonTemplates(ref string wikiText)
        {
            var matches = Regex.Matches(wikiText, "{{выдел[|]([^{}]+)}}");
            foreach (Match match in matches)
            {
                wikiText = wikiText.Replace(match.Value, match.Groups[1].Value);
            }

            wikiText = wikiText.Replace("{{итп}}", "и т. п.");
            wikiText = wikiText.Replace("{{итд}}", "и т. д.");

            wikiText = wikiText.Replace("{{-}}", " — ");

            wikiText = Regex.Replace(wikiText, @"{{мн[\.]*}}", "мн. ч.");
            wikiText = Regex.Replace(wikiText, @"{{ед[\.]*}}", "ед. ч.");

            wikiText = wikiText.Replace(@"'''", "");
            wikiText = wikiText.Replace(@"''", "");
        }

        //Removes all useless prefixes
        private static void RemovePrefixes(ref string wikiText)
        {
            var matches = Regex.Matches(wikiText, "{{[^{}]+}}");
            if (matches.Count == 0) return;
            foreach (Match match in matches)
            {
                wikiText = wikiText.Replace(match.Value, "");
            }
        }

        //Removes any HTML code from wikitext
        public static void RemoveHtmlCode(ref string wikiText)
        {
            wikiText = wikiText.Replace("<br />", "");
            wikiText = wikiText.Replace("<br/>", "");
            wikiText = wikiText.Replace("<br>", "");

            //HTML comments
            wikiText = Regex.Replace(wikiText, "<!--.*-->", "");

            //HTML tags
            var tags = new[]
            {
                "<ref>(.+)</ref>",
                "<u>(.+)</u>",
                "<s>(.+)</s>",
                "<small>(.+)</small>",
                "<div>(.+)</div>",
                "<big>(.+)</big>",
                "<sub>(.+)</sub>",
                "<sup>(.+)</sup>",
                "<blockquote>(.+)</blockquote>",
                "<code>(.+)</code>",
                "<pre>(.+)</pre>"
            };
            foreach (var tag in tags)
            {
                var matches = Regex.Matches(wikiText, tag);
                if (matches.Count == 0) continue;
                foreach (Match match in matches)
                    wikiText = wikiText.Replace(match.Value, match.Groups[1].Value);
            }
        }

    }
}