﻿using System.Collections.Generic;

namespace Dictionary
{
    public class Meaning
    {
        public string Text { get; set; }
        public List<string> Hyperonims { get; set; }
        public Meaning(string text, List<string> hyperonims)
        {
            Text = text;
            Hyperonims = hyperonims;
        }
    }
}