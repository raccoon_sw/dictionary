Для корректной работы приложения нужно распаковать файл mystem-3.1-win-64bit.zip
желательно в корень диска C. Текстовые файлы желательно размещать там же
(т.к. полный путь к файлу не должен содержать кириллические символы)
Также в конфигурационном файле App.config прописать необходимые данные,
а именно:
PathMyStem - полный путь до исполняемого файла mystem.exe
connectionString, где Database - имя базы данных Викисловаря
                      User Id - имя пользователя, имеющего все права на БД
                      Password - пароль этого пользователя